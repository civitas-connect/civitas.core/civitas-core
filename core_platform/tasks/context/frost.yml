---
- name: "Frost: Set default facts for K8S"
  ansible.builtin.set_fact:
    k8s_config: "{{ K8S_KUBECONFIG_PATH }}/{{ inv_cm.frost.ns_kubeconfig }}"
    k8s_context: "{{ inv_k8s.config.context }}"
    k8s_namespace: "{{ inv_cm.frost.ns_name }}"

- name: "Frost: Create namespace {{ k8s_namespace }}"
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition:
      apiVersion: v1
      kind: Namespace
      metadata:
        name: "{{ k8s_namespace }}"
        labels:
          name: "{{ k8s_namespace }}"
  when: inv_cm.frost.ns_create

- name: "Frost: Ensure gitlab-registry secret is available"
  include_tasks: tasks/templates/registry_secret.yml

- name: "Frost: Get Admin credentials"
  kubernetes.core.k8s_info:
    namespace: "{{ inv_access.ns_name | lower }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    kind: Secret
    name: "{{ inv_access.keycloak.k8s_secret_name }}"
  register: admin_credentials

- name: "Frost: Store IDM Credentials"
  set_fact:
    ADMIN_USERNAME: "{{ admin_credentials.resources[0].data.MASTER_USERNAME | b64decode }}"
    ADMIN_PASSWORD: "{{ admin_credentials.resources[0].data.MASTER_PASSWORD | b64decode }}"
    cacheable: yes

- name: "Frost: Re-Login to keycloak"
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/realms/master/protocol/openid-connect/token"
    return_content: yes
    ca_path: "{{ inv_k8s.ingress.ca_path }}"
    body_format: form-urlencoded
    body:
      grant_type: "password"
      client_id: "admin-cli"
      username: "{{ ADMIN_USERNAME }}"
      password: "{{ ADMIN_PASSWORD }}"
    status_code: 200
  register: keycloak_login

- name: "Frost: Get OIDC Values"
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/realms/{{ inv_access.keycloak.realm_name }}/.well-known/openid-configuration"
    return_content: yes
    ca_path: "{{ inv_k8s.ingress.ca_path }}"
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: keycloak_oidc_response

- name: "Frost: Get OIDC Values"
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/realms/{{ inv_access.keycloak.realm_name }}"
    return_content: yes
    ca_path: "{{ inv_k8s.ingress.ca_path }}"
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: keycloak_key_response

- name: "Frost: Get keycloak public signing key."
  set_fact:
    public_signing_key: "{{ keycloak_key_response.json.public_key }}"

- name: "Frost: Deploy PostgresSQL database for Frost via Zalando operator"
  kubernetes.core.k8s:
    state: present
    namespace: "{{ inv_context.frost.ns_name }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    definition: "{{ item }}"
  loop:
    - "{{ lookup('template', 'templates/context/frost/frost-db.yaml') | from_yaml }}"
  when: inv_cm.frost.db.enable

- name: "Frost: Prepare Frost Secrets from own DB Cluster"
  block:
    - name: "Frost: Get Frost Postgres credentials"
      kubernetes.core.k8s_info:
        kind: Secret
        namespace: "{{ inv_cm.frost.ns_name }}"
        kubeconfig: "{{ k8s_config }}"
        context: "{{ k8s_context }}"
        name: "{{ db_common.frost_postgres.user.k8s_secret }}"
        wait: yes
        wait_sleep: 10
        wait_timeout: 1
      register: frost_db_admin

    - name: "Frost: Store Frost Postgres Admin Credentials"
      set_fact:
        FROST_POSTGRES_DATABASE: "frost"
        FROST_POSTGRES_EXTERNAL_ADDRESS: "{{ 'context-frost.' + inv_cm.frost.ns_name + '.svc.cluster.local' }}"
        FROST_POSTGRES_HOST: "{{ 'context-frost.' + inv_cm.frost.ns_name }}"
        FROST_POSTGRES_PORT: "{{ inv_cm.frost.db.port | default('5432') }}"
        FROST_POSTGRES_PASSWORD: "{{ frost_db_admin.resources[0].data.password | b64decode }}"
        FROST_POSTGRES_USERNAME: "{{ frost_db_admin.resources[0].data.username | b64decode }}"
  when: inv_cm.frost.db.enable

- name: "Frost: Prepare Frost Secrets from central DB Cluster"
  block:
    - name: "Frost: Get Keycloak Postgres credentials"
      kubernetes.core.k8s_info:
        kind: Secret
        namespace: "{{ inv_central_db.ns_name }}"
        kubeconfig: "{{ k8s_config }}"
        context: "{{ k8s_context }}"
        name: "{{ db_common.frost_central_db.user.k8s_secret }}"
        wait: yes
        wait_sleep: 10
        wait_timeout: 1
      register: frost_db_admin

    - name: "Frost: Store Frost Postgres Admin Credentials"
      set_fact:
        FROST_POSTGRES_DATABASE: "frost"
        FROST_POSTGRES_EXTERNAL_ADDRESS: "{{ 'central-db.' + inv_central_db.ns_name + '.svc.cluster.local' }}"
        FROST_POSTGRES_HOST: "{{ 'central-db.' + inv_central_db.ns_name }}"
        FROST_POSTGRES_PORT: "{{ inv_central_db.port | default('5432') }}"
        FROST_POSTGRES_PASSWORD: "{{ frost_db_admin.resources[0].data.password | b64decode }}"
        FROST_POSTGRES_USERNAME: "{{ frost_db_admin.resources[0].data.username | b64decode }}"
  when: inv_cm.frost.db.central

- name: "Frost: Prepare Frost Secrets from managed Database"
  block:
    - name: "Frost: Get Frost Postgres credentials"
      kubernetes.core.k8s_info:
        kind: Secret
        namespace: "{{ inv_mngd_db.frost_postgres.ns_name }}"
        kubeconfig: "{{ k8s_config }}"
        context: "{{ k8s_context }}"
        name: "{{ inv_mngd_db.frost_postgres.user_k8s_secret }}"
        wait: yes
        wait_sleep: 10
        wait_timeout: 1
      register: frost_db_admin

    - name: "Frost: Store Frost Postgres Admin Credentials"
      set_fact:
        FROST_POSTGRES_DATABASE: "{{ inv_mngd_db.frost_postgres.db_name }}"
        FROST_POSTGRES_EXTERNAL_ADDRESS: "{{ inv_mngd_db.frost_postgres.db_address }}"
        FROST_POSTGRES_HOST: "{{ inv_mngd_db.frost_postgres.db_name }}"
        FROST_POSTGRES_PORT: "{{ inv_mngd_db.frost_postgres.port | default('5432') }}"
        FROST_POSTGRES_PASSWORD: "{{ frost_db_admin.resources[0].data.password | b64decode }}"
        FROST_POSTGRES_USERNAME: "{{ frost_db_admin.resources[0].data.username | b64decode }}"
  when: inv_mngd_db.frost_postgres.enable

- name: "Frost: Define initial database facts"
  set_fact:
    frostdb:
      2:
        Name: "FROST"
        Group: "CONTEXT-STACK"
        Host: "{{ FROST_POSTGRES_HOST }}"
        Port: 5432
        MaintenanceDB: "postgres"
        DBRestriction: "{{ FROST_POSTGRES_DATABASE }}"
        Username: "{{ FROST_POSTGRES_PASSWORD }}"
        SSLMode: "require"
        Shared: true
        SharedUsername: "{{ FROST_POSTGRES_USERNAME }}"
    cacheable: yes

- name: "Frost: Add new database to database facts"
  set_fact:
    core_platform_databases: "{{ core_platform_databases + [frostdb] }}"

- name: "Frost: Create ConfigMap from CA certificate"
  kubernetes.core.k8s:
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    state: present
    definition:
      apiVersion: v1
      kind: ConfigMap
      metadata:
        name: cacert
        namespace: "{{ k8s_namespace }}"
      data:
        cacert.crt: |
          {{ lookup('file', inv_k8s.ingress.ca_path) }}
  when: inv_k8s.ingress.ca_path is defined and inv_k8s.ingress.ca_path != ""

- name: "Frost: Create the Context-Management FROST-Server Helm Installation"
  include_tasks: tasks/templates/k8s-helm.yml
  vars:
    helm_repo_name: "{{ software.stack_cm.frost.helm_repo_name }}"
    helm_repo_url: "{{ software.stack_cm.frost.helm_repo_url }}"
    helm_chart_name: "{{ software.stack_cm.frost.helm_chart_name }}"
    helm_release_name: "{{ software.stack_cm.frost.helm_release_name }}"
    helm_chart_reference: "{{ helm_repo_name }}/{{ helm_chart_name }}"
    helm_chart_version: "{{ software.stack_cm.frost.helm_chart_version }}"
    helm_values_file: "templates/context/frost/frost_values.yaml"
    helm_create_namespace: false

- name: "Frost: Wait for FROST deployment to be ready"
  kubernetes.core.k8s_info:
    kind: Deployment
    namespace: "{{ k8s_namespace }}"
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    label_selectors: "app.kubernetes.io/instance=frost-server"
    wait: yes
    wait_sleep: 10
    wait_timeout: 600
    wait_condition:
      type: Available
      status: True

- name: "Frost: Create velero Backup schedule namespace {{ k8s_namespace }}"
  kubernetes.core.k8s:
    state: present
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ inv_op_stack.velero.ns_name }}"
    definition:
      apiVersion: velero.io/v1
      kind: Schedule
      metadata:
        name: "{{ k8s_namespace }}-backup"
      spec:
        schedule: 0 2 * * *
        template:
          defaultVolumesToRestic: true
          hooks: {}
          includedNamespaces:
            - "{{ k8s_namespace }}"
          metadata: {}
          snapshotVolumes: true
          storageLocation: "{{ inv_op_stack.velero.backup.location_name }}-backup"
          ttl: 720h0m0s
  when: inv_op_stack.velero.enable == "true"

# Patch MQTT Service to use SessionAffinity None
- name: "Frost: Patch Session Affinity if required"
  when: inv_cm.frost.mqtt.session_affinity is defined and not inv_cm.frost.mqtt.session_affinity == "ClientIP"
  kubernetes.core.k8s:
    kubeconfig: "{{ k8s_config }}"
    context: "{{ k8s_context }}"
    namespace: "{{ k8s_namespace }}"
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: frost-server-frost-server-mqtt
        namespace: "{{ k8s_namespace }}"
      spec:
        sessionAffinity: "{{ inv_cm.frost.mqtt.session_affinity }}"

- name: "Frost: Authenticate admin user via OIDC at IDM"
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/realms/{{ inv_access.keycloak.realm_name }}/protocol/openid-connect/token"
    body:
      grant_type: "password"
      client_id: "api-access"
      client_secret: "{{ IDM_CLIENT_SECRET_API_ACCESS }}"
      username: "{{ inv_access.platform.master_username }}"
      password: "{{ inv_access.platform.master_password }}"
      scope: "api:read api:write api:delete"
    body_format: form-urlencoded
    headers:
      Content-Type: "application/x-www-form-urlencoded"
    status_code: 200
  register: oidc_token

- name: "Frost: Find the correct Frost HTTP server pod"
  command: >
    kubectl get pod -n "{{ k8s_namespace }}"
    --kubeconfig "{{ k8s_config }}" --context "{{ k8s_context }}"
    -l "app=frost-server,app.kubernetes.io/instance=frost-server,component=http"
    -o jsonpath='{.items[0].metadata.name}'
  register: frost_pod
  changed_when: false
  failed_when: frost_pod.rc != 0 or frost_pod.stdout == ""

- name: "Frost: Fetch existing data spaces"
  command: >
    kubectl exec -n "{{ k8s_namespace }}" {{ frost_pod.stdout }}
    --kubeconfig "{{ k8s_config }}" --context "{{ k8s_context }}" --
    curl -s -X GET "http://localhost:8080/FROST-Server/v1.1/Projects"
    -H "Authorization: Bearer {{ oidc_token.json.access_token }}"
  register: project_check
  changed_when: false
  failed_when: project_check.rc != 0

- name: "Frost: Parse JSON response"
  set_fact:
    project_json: "{{ project_check.stdout | from_json }}"

- name: "Frost: Extract existing project names"
  set_fact:
    existing_projects: "{{ project_json.value | map(attribute='name') | list }}"

- name: "Frost: Check if Open Data Space already exists"
  set_fact:
    project_exists: "{{ inv_access.open_data_dataspace | default('ds_open_data') in existing_projects }}"

- name: "Frost: Create Open Data Space only if missing"
  command: >
    kubectl exec -n "{{ k8s_namespace }}" {{ frost_pod.stdout }}
    --kubeconfig "{{ k8s_config }}" --context "{{ k8s_context }}" --
    curl -s -o /tmp/curl_response.json -w "%{http_code}"
    -X POST "http://localhost:8080/FROST-Server/v1.1/Projects"
    -H "Content-Type: application/json"
    -H "Authorization: Bearer {{ oidc_token.json.access_token }}"
    -d '{
        "name": "{{ inv_access.open_data_dataspace | default('ds_open_data') }}",
        "description": "This is the open data space of the platform.",
        "public": true
    }'
  when: not project_exists
  register: curl_result
  changed_when: not project_exists
  failed_when: curl_result.stdout|int != 201

- name: "Frost: Filter out already existing additional data spaces"
  set_fact:
    new_additional_spaces: "{{ inv_access.additional_dataspaces | difference(existing_projects) }}"
  when: inv_access.additional_dataspaces is defined and inv_access.additional_dataspaces | length > 0

- name: "Frost: Create Additional Data Space if missing"
  command: >
    kubectl exec -n "{{ k8s_namespace }}" {{ frost_pod.stdout }}
    --kubeconfig "{{ k8s_config }}" --context "{{ k8s_context }}" --
    curl -s -o /tmp/curl_response.json -w "%{http_code}"
    -X POST "http://localhost:8080/FROST-Server/v1.1/Projects"
    -H "Content-Type: application/json"
    -H "Authorization: Bearer {{ oidc_token.json.access_token }}"
    -d '{
        "name": "{{ item }}",
        "description": "This is an additional data space."
    }'
  loop: "{{ new_additional_spaces }}"
  register: curl_results
  changed_when: new_additional_spaces | length > 0
  failed_when: curl_results.stdout is not defined or curl_results.stdout | int != 201
  when: inv_access.additional_dataspaces is defined and inv_access.additional_dataspaces | length > 0 and new_additional_spaces | length > 0
