#!/usr/bin/env bash

# Define source and target paths
HOST_KUBE_DIR="/host-home-folder/.kube"
HOST_SSH_DIR="/host-home-folder/.ssh"
USER_HOME="$HOME"  # This automatically sets the user's home directory

# Check if the .kube directory exists in /host-home-folder and link it
if [ -d "$HOST_KUBE_DIR" ]; then
  ln -s "$HOST_KUBE_DIR" "$USER_HOME/.kube"
  echo "Linked $HOST_KUBE_DIR to $USER_HOME/.kube"
else
  echo "$HOST_KUBE_DIR does not exist, skipping .kube link."
fi

# Check if the .ssh directory exists in /host-home-folder and link it
if [ -d "$HOST_SSH_DIR" ]; then
  ln -s "$HOST_SSH_DIR" "$USER_HOME/.ssh"
  echo "Linked $HOST_SSH_DIR to $USER_HOME/.ssh"
else
  echo "$HOST_SSH_DIR does not exist, skipping .ssh link."
fi
