# List of contributors
Thanks to everyone who contributed to this project or previous works!

Contributors since the initial version in 2019 until 2023 ([-> source](https://gitlab.com/berlintxl/futr-hub/getting-started/-/blob/master/LIST-OF-CONTRIBUTORS.md?ref_type=heads)):
- Andreas Linneweber
- Dr. Markus Luckey
- Thomas Haarhoff
- Anton Hannemann
- Dennis Sitelew
- Dr. Jan Stehr
- Michael Gollan
- Olaf Heinemann
- Dr. Stefan Höffken
- Mathias Renner
- Thomas Schweppe
- Elias Schneider
- Nora König
- Robin Lamberti
- Ferdinand Muetsch
- Will Freitag
- Finn Fentker
- Christian Martens
- Marcus Graetsch
- Stefanie Wiemer
- Konrad Kessler

Additional contributors since 2024:
- Lucas Bojer
- Dr. Anne Bumiller
- Michel Barnisch
- Fabian Ifflaender
- Valerie George
- Jan Goden
- Fritz Hoing ([@FritzHoing](https://gitlab.com/FritzHoing), https://www.terrestris.de/)
- Jan-Philipp Litza ([@jplitza](https://gitlab.com/jplitza), https://jplitza.de/)