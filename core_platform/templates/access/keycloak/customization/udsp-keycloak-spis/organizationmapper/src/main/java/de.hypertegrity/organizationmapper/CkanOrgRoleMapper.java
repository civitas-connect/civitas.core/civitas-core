/*
 * This is a custom OIDC protocol mapper that extends the AbstractOIDCProtocolMapper
 * class and implements the OIDCAccessTokenMapper interface.
 * It adds custom claims to the OIDC access token based on the user's groups and client roles.
 * 
 */

package de.hypertegrity.organizationmapper;

import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.ClientModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.oidc.mappers.AbstractOIDCProtocolMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper;
import org.keycloak.protocol.oidc.mappers.OIDCIDTokenMapper;
import org.keycloak.protocol.oidc.mappers.UserInfoTokenMapper;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.IDToken;

import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class CkanOrgRoleMapper extends AbstractOIDCProtocolMapper
	implements OIDCAccessTokenMapper, OIDCIDTokenMapper, UserInfoTokenMapper {

	public static final String PROVIDER_ID = "ckan-org-role-mapper";

	private static final List<ProviderConfigProperty> configProperties = new ArrayList<>();
	private static final Logger logger = Logger.getLogger(CkanOrgRoleMapper.class); // Create a Logger instance

	static {
		OIDCAttributeMapperHelper.addTokenClaimNameConfig(configProperties);
		OIDCAttributeMapperHelper.addIncludeInTokensConfig(configProperties, CkanOrgRoleMapper.class);
	}

	@Override
	public String getId() {
		return PROVIDER_ID;
	}

	@Override
	public String getDisplayCategory() {
		return TOKEN_MAPPER_CATEGORY;
	}

	@Override
	public String getDisplayType() {
		return "CKAN Org Role Mapper";
	}

	@Override
	public String getHelpText() {
		return "Adds CKAN Organization and Roles to token claim";
	}

	@Override
	public List<ProviderConfigProperty> getConfigProperties() {
		return configProperties;
	}


    @Override
    public void setClaim(IDToken token, ProtocolMapperModel mappingModel, UserSessionModel userSession,
                         KeycloakSession keycloakSession, ClientSessionContext clientSessionCtx) {

        /*
         * This method is called to set the custom claim "ckan_roles" in the OIDC access token.
         * It retrieves the user's groups, checks for specific group names ("admin", "editor", "member"),
         * and constructs a list of roles based on these groups.
         * It also checks if the user has the "ckanAdmin" client role and includes it in the "ckan_roles" list.
         * Finally, the "ckan_roles" list is added to the token's other claims.
         */
        List<Object> ckanRoles = new ArrayList<>();
        List<GroupModel> userGroups = userSession.getUser().getGroupsStream().collect(Collectors.toList());

        for (GroupModel group : userGroups) {
            String orgName = null;
            if (group.getParent() != null) {
                // Check if the parent group has the "org" attribute
                Map<String, List<String>> parentAttributes = group.getParent().getAttributes();
                if (parentAttributes.containsKey("org") && parentAttributes.get("org").contains("true")) {
                    // If the parent has an attribute "orgName," use its value; otherwise, use the group name
                    orgName = parentAttributes.getOrDefault("orgname", Collections.singletonList(group.getParent().getName())).get(0);
                }
            }

            String groupName = group.getName();

            if ("admin".equals(groupName) || "editor".equals(groupName) || "member".equals(groupName)) {
                ckanRoles.add(Map.of("role", groupName, "org", orgName));
            }
        }

        // Check if the user has the client role "ckanAdmin"
        boolean hasCkanAdminRole = clientSessionCtx.getRolesStream()
                .map(RoleModel::getName)
                .anyMatch(roleName -> roleName.equals("ckanAdmin"));

        // Add "ckanAdmin" to ckanRoles if the user has the "ckanAdmin" client role
        if (hasCkanAdminRole) {
            ckanRoles.add("ckanAdmin");
        }

        // Add the "ckan_roles" to the token
        token.getOtherClaims().put("ckan_roles", ckanRoles);
    }
}