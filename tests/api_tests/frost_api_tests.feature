Feature: FROST Server API Tests

  Scenario: Test API Availability
    * url java.lang.System.getenv('KEYCLOAK_BASE_URL') + '/auth/realms/' + java.lang.System.getenv('REALM') + '/protocol/openid-connect/token'
    * form field client_id = 'api-access'
    * form field client_secret = java.lang.System.getenv('API_ACCESS_CLIENT_SECRET')
    * form field username = java.lang.System.getenv('TESTUSER_USERNAME')
    * form field password = java.lang.System.getenv('TESTUSER_PASSWORD')
    * form field grant_type = 'password'
    * form field scope = 'api:read'
    * header Content-Type = 'application/x-www-form-urlencoded'
    * method post
    * status 200, 'Test failed because the token could not be retrieved from Keycloak'
    * def accessToken = response.access_token
    Given url java.lang.System.getenv('FROST_API_URL');
    * header Authorization = 'Bearer ' + accessToken
    * path '/v1.1'
    When method get
    Then status 200 
    Then assert typeof response == 'object'
        


  Scenario: Test API Authentication 
    Given url java.lang.System.getenv('FROST_API_URL');
    * header Authorization = 'Bearer invalid-token'
    * path '/v1.1'
    When method get
    Then status 401


  Scenario: Test API Authorization (missing scope api:read)
    * url java.lang.System.getenv('KEYCLOAK_BASE_URL') + '/auth/realms/' + java.lang.System.getenv('REALM') + '/protocol/openid-connect/token'
    * form field client_id = 'api-access'
    * form field client_secret = java.lang.System.getenv('API_ACCESS_CLIENT_SECRET')
    * form field username = java.lang.System.getenv('TESTUSER_USERNAME')
    * form field password = java.lang.System.getenv('TESTUSER_PASSWORD')
    * form field grant_type = 'password'
    * header Content-Type = 'application/x-www-form-urlencoded'
    * method post
    * status 200, 'Test failed because the token could not be retrieved from Keycloak'
    * def accessToken = response.access_token
    Given url java.lang.System.getenv('FROST_API_URL');
    * header Authorization = 'Bearer ' + accessToken
    * path '/v1.1'
    When method get
    Then status 403 