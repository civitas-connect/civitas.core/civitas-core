# yaml-language-server: $schema=https://gitlab.com/civitas-connect/civitas-core/civitas-core/-/raw/main/core_platform/inventory_schema.json

###########################################################
# Inventory for the Open City Platform
###########################################################

all:
  vars:
    DOMAIN: "civitas.test"
    ENVIRONMENT: 'tst'
    kubeconfig_file: config

  children:
    controller:
      hosts:
        localhost:
          ansible_host: 127.0.0.1
          ansible_connection: local
          ansible_python_interpreter: "{{ ansible_playbook_python }}"

      vars:

        ###########################################################
        ## Kubernetes general settings
        ###########################################################

        inv_k8s:
          config:
            context: "k3d-civitas-local"

          storage_class:
            rwo: "local-path"
            rwx: "local-path"
            loc: "local-path"

          ingress:
            ca_path: "/workspaces/civitas-core/local_deployment/.ssl/civitas.crt" # Path to your local self-signed CA certificate
            http: false # Allow http access (without SSL)

          cert_manager:
            issuer_name: "selfsigned-ca"

        ###########################################################
        ## Operation stack
        ## The operation stack will be the place, where all management components will live.
        ###########################################################

        inv_op_stack:
          prometheus: # sammelt Log-Daten über die Umgebung. Kann man dann über Grafana angucken
            enable: false

          # Keel Operator automatically updates docker images
          keel_operator:
            enable: false
            admin: "admin@{{ DOMAIN }}"
            password: "cIv!Ta$_C0R3"

          # PGAdmin to manage postgres databases
          pgadmin:
            enable: false
            default_email: "admin@{{ DOMAIN }}"
            default_password: "cIv!Ta$_C0R3"

          # Monitoring stack with prometheus, grafana, alertmanager, loki and promtail
          monitoring:
            enable: false
            prometheus:
              enable: true
            grafana:
              enable: true
            alertmanager:
              enable: true
            loki:
              enable: true
            promtail:
              enable: true


        ###########################################################
        ## Access management stack
        ## Identity and api management.
        ###########################################################

        inv_access:
          enable: true

          ## IDM (keycloak) Users
          platform:
            admin_first_name: "Admin"
            admin_surname: "Admin"
            admin_email: "admin@{{ DOMAIN }}"
            master_username: "admin@{{ DOMAIN }}"
            master_password: "cIv!Ta$_C0R3"

          # API management
          apisix:
            enable: true
            stream: # enable for MQTT Integration
              enable: true

          service_portal:
            enable: true
            certs:
              enable: true
            oidc:
              enable: false

        ###########################################################
        ## Context Management
        ## context management components, i.e., Frost, Stellio, Mimir, ...
        ###########################################################

        inv_cm:
          frost:
            enable: true
            mqtt:
              enable: true
              session_affinity: "None"


        ###########################################################
        ## Dashboards: Grafana and Superset
        ###########################################################

        inv_da:
          superset:
            enable: false
            mapbox_api_token: "TODO_PLEASE_SET_A_VALUE"
            # Generate once per System with > openssl rand -base64 42
            db_secret: "1Zeq0k+zSsUMvklT5Jayksj4NBKJ4JKo2hYwCB1khpoc2Z3AmhZJ3pTZ"
            admin_user_name: admin
            admin_user_password: "cIv!Ta$_C0R3"
            redis_auth_password: "cIv!Ta$_C0R3"

        ###########################################################
        ## GeoData Stack
        ###########################################################

        inv_gd:
          enable: false

          # Masterportal in Version 2.xx
          masterportal_1:
            enable: true

          # Mapfish to generate PDF maps from masterportal
          mapfish:
            enable: false

          # GeoServer
          geoserver:
            enable: true

          # Portal Backend to support authentication in MaserPortal
          portal_backend:
            enable: true

        ###########################################################
        ## AddOns
        ## Platform addons can be added and configured here. See example addon repository for documentation
        ###########################################################

        inv_addons:
          import: false
          addons: []
          # - 'addons/mimir_addon/tasks.yml'
