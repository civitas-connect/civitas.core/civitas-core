#!/usr/bin/env bash

bash /scripts/set_python_env.sh $PYTHON_ENV

pip install --upgrade pip

pip install -r /requirements/requirements.txt

ansible-galaxy collection install -r /requirements/ansible-collections.yml

curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt install -y nodejs
node -v
npm -v
sudo npm install -g ajv-cli

echo

bash /scripts/setup_home.sh
