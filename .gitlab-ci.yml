#
# GITLAB CI Variables:
# --------------------
#

stages:
  # - verify
  - dev
  - staging
  - prod
  - test
  - vulnerability_scanning
  - dependency_scanning

image: willhallonline/ansible:2.10-ubuntu-20.04

variables: &global-variables
  ANSIBLE_HOST_KEY_CHECKING: "false"
  ANSIBLE_FORCE_COLOR: "true"
  ANSIBLE_CONFIG: "ansible.cfg"
  ANSIBLE_SCP_IF_SSH: "y"
  DEBIAN_FRONTEND: "noninteractive"

####################################
######    GENERAL SCRIPTS   ########
####################################

.pre_deploy: &pre_deploy
  - apt-get update -y && apt-get install curl apt-transport-https -y
  # Add Helm package repo
  - curl https://baltocdn.com/helm/signing.asc | apt-key add -
  - echo "deb https://baltocdn.com/helm/stable/debian/ all main" | tee /etc/apt/sources.list.d/helm-stable-debian.list
  # Add kubectl package repo
  - curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
  - echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | tee /etc/apt/sources.list.d/kubernetes.list
  # Install required system packages
  - apt-get update -y && apt install openssh-client curl vim git less iputils-ping sshpass libpq-dev gcc python3-dev musl-dev helm kubectl -y
  # Install required python packages
  - python3 -m pip install jmespath docker psycopg2-binary 'pymongo==3.12.1' 'openshift==0.12.1'
  - ansible-galaxy install -r requirements.yml

####################################
######     VERIFY / LINT    ########
####################################

### verify syntaxs etc...
#verify_commit:
#  stage: verify
#  environment:
#    name: "Verify ansible playbooks"
#  script:
#    # Install required python packages
#    - python3 -m pip install 'openshift==0.11' jmespath docker psycopg2-binary pymongo
#    - ansible-galaxy collection install community.kubernetes
#    - ansible-galaxy install -r requirements.yml
#    - 'which ansible-lint || ( pip install ansible-lint )'
#    - ansible-lint -x experimental -v 03_setup_k8s_platform/full_install.yml

####################################
######         DEV       ###########
####################################

.prepare_deploy_dev: &prepare_deploy_dev
  - *pre_deploy
  - mv "$INVENTORY_DEV" inventory
  - mkdir -p /root/.kube
  - mv "$KUBECONFIG_DEV" /root/.kube/admin.conf
  - chmod 600 /root/.kube/admin.conf
  - export ANSIBLE_LIBRARY="root/.ansible/plugins/modules:~/.ansible/plugins/modules:/usr/share/ansible/plugins/modules"
  - export ANSIBLE_COLLECTIONS_PATH="root/.ansible/collections:~/.ansible/collections:/usr/share/ansible/collections"

deploy_dev_platform:
  before_script:
    - git submodule update --init --recursive
  script:
    - *prepare_deploy_dev
    - ansible-playbook -i inventory -l localhost 02_core_platform/full_install.yml --skip-tags ml_loki_promtail,ml_grafana
  when: manual
  stage: dev
  environment:
    name: "Deploy DEV Platform"

# ####################################
# ######       STAGING     ###########
# ####################################

# .prepare_deploy_staging: &prepare_deploy_staging
#     - *pre_deploy
#     - mv "$INVENTORY_STG" inventory
#     - mkdir -p /root/.kube
#     - mv "$KUBECONFIG_STG" /root/.kube/admin.conf
#     - chmod 600 /root/.kube/admin.conf
#     - export ANSIBLE_LIBRARY="root/.ansible/plugins/modules:~/.ansible/plugins/modules:/usr/share/ansible/plugins/modules"
#     - export ANSIBLE_COLLECTIONS_PATH="root/.ansible/collections:~/.ansible/collections:/usr/share/ansible/collections"

# deploy_staging_platform:
#   before_script:
#     - git submodule update --init --recursive
#   script:
#     - *prepare_deploy_staging
#     - ansible-playbook -i inventory -l localhost 02_core_platform/full_install.yml --skip-tags ml_loki_promtail,ml_grafana
#   when: manual
#   stage: staging
#   environment:
#     name: "Deploy STAGING Platform"

# ####################################
# ######        PROD       ###########
# ####################################

# .prepare_deploy_prod: &prepare_deploy_prod
#     - *pre_deploy
#     - mv "$INVENTORY_PRD" inventory
#     - mkdir -p /root/.kube
#     - mv "$KUBECONFIG_PRD" /root/.kube/admin.conf
#     - chmod 600 /root/.kube/admin.conf
#     - export ANSIBLE_LIBRARY="root/.ansible/plugins/modules:~/.ansible/plugins/modules:/usr/share/ansible/plugins/modules"
#     - export ANSIBLE_COLLECTIONS_PATH="root/.ansible/collections:~/.ansible/collections:/usr/share/ansible/collections"

# deploy_prod_platform:
#   before_script:
#     - git submodule update --init --recursive
#   script:
#     - *prepare_deploy_prod
#     - ansible-playbook -i inventory -l localhost 02_core_platform/full_install.yml --skip-tags ml_loki_promtail,ml_grafana
#   when: manual
#   stage: prod
#   environment:
#     name: "Deploy PROD Platform"

####################################
###### VULNERABILITY SCANNING ######
####################################

vulnerability_scanning:
  stage: vulnerability_scanning
  needs: []
  image:
    name: $CI_REGISTRY/civitas-connect/civitas-core/external-image-security-scanning:latest
    entrypoint: [""]
  variables:
    SOFTWARE_REFERENCES_PATH: $CI_PROJECT_DIR/core_platform/vars/software_references.yml
  script:
    - cd /app/
    - ./create_reports.sh
  after_script:
    - mv /app/reports $CI_PROJECT_DIR
  cache:
    key: trivy-cache
    paths:
      - /app/tmp/trivy/
  artifacts:
    reports:
      container_scanning: reports/gl-container-scanning-report.json
      cyclonedx: "reports/gl-sbom-*.cdx.json"
    paths:
      [
        "reports/gl-container-scanning-report.json",
        "reports/gl-sbom-*.cdx.json",
      ]
  environment:
    name: "Vulnerability Scanning"
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' || $CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

dependency_scanning:
  stage: dependency_scanning
  needs: []
  image: renovate/renovate:slim
  variables:
    RENOVATE_PLATFORM: gitlab
    RENOVATE_ENDPOINT: $CI_API_V4_URL
    RENOVATE_CONFIG_FILE: $CI_PROJECT_DIR/renovate/renovate.json
    RENOVATE_BINARY_SOURCE: install
    LOG_LEVEL: debug
  script:
    - renovate $CI_PROJECT_PATH $RENOVATE_EXTRA_FLAGS
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
