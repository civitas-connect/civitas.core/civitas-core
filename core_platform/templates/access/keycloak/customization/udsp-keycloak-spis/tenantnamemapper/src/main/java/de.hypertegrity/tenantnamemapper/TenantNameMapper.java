package de.hypertegrity.tenantnamemapper;

import org.keycloak.models.ClientSessionContext;
import org.keycloak.models.ClientModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.ProtocolMapperModel;
import org.keycloak.models.RoleModel;
import org.keycloak.models.UserSessionModel;
import org.keycloak.protocol.oidc.mappers.AbstractOIDCProtocolMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAccessTokenMapper;
import org.keycloak.protocol.oidc.mappers.OIDCAttributeMapperHelper;
import org.keycloak.protocol.oidc.mappers.OIDCIDTokenMapper;
import org.keycloak.protocol.oidc.mappers.UserInfoTokenMapper;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.representations.IDToken;

import org.jboss.logging.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.Collectors;

public class TenantNameMapper extends AbstractOIDCProtocolMapper
	implements OIDCAccessTokenMapper, OIDCIDTokenMapper, UserInfoTokenMapper {

	public static final String PROVIDER_ID = "tenant-name-mapper";

	private static final List<ProviderConfigProperty> configProperties = new ArrayList<>();
	private static final Logger logger = Logger.getLogger(TenantNameMapper.class);

	static {
		OIDCAttributeMapperHelper.addTokenClaimNameConfig(configProperties);
		OIDCAttributeMapperHelper.addIncludeInTokensConfig(configProperties, TenantNameMapper.class);
	}

	@Override
	public String getId() {
		return PROVIDER_ID;
	}

	@Override
	public String getDisplayCategory() {
		return TOKEN_MAPPER_CATEGORY;
	}

	@Override
	public String getDisplayType() {
		return "Tenant Name Mapper";
	}

	@Override
	public String getHelpText() {
		return "Adds Dataspace Tenant Names to token claim";
	}

	@Override
	public List<ProviderConfigProperty> getConfigProperties() {
		return configProperties;
	}


    @Override
    public void setClaim(IDToken token, ProtocolMapperModel mappingModel, UserSessionModel userSession,
                         KeycloakSession keycloakSession, ClientSessionContext clientSessionCtx) {

        List<Object> tenants = new ArrayList<>();
        List<GroupModel> userGroups = userSession.getUser().getGroupsStream().collect(Collectors.toList());

        // Iterate all groups of the user
        for (GroupModel group : userGroups) {
            // Read attributes of that group
            Map<String, List<String>> groupAttributes = group.getAttributes();
            if (groupAttributes.containsKey("service") && groupAttributes.get("service").contains("true")) {
                if (groupAttributes.containsKey("tenant") && groupAttributes.get("tenant").contains("true")) {
                    // If attributes 'service' and 'tenant' are set to true, add the group to the list of tenants
                    tenants.add(Map.of("name", group.getName()));
                }
            }
        }
        // Add the "tenants" to the token
        token.getOtherClaims().put("tenants", tenants);
    }
}