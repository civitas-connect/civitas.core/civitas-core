
<div align="center">
    <br />
    <a href="https://www.civitasconnect.digital/civitas-core/">
      <img width="350px" src="https://www.civitasconnect.digital/wp-content/uploads/2024/03/CIVITASCORE_1-zeilig-mit-Icon-oben.png" alt="CIVITAS/CORE Logo">
    </a>
    <br />
    <br />
</div>


[![Latest Release](https://gitlab.com/civitas-connect/civitas.core/civitas-core/-/badges/release.svg)](https://gitlab.com/civitas-connect/civitas.core/civitas-core/-/releases) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)

This repository provides an automated setup for the CIVITAS/CORE platform within a Kubernetes environment.

> Für Informationen in deutscher Sprache besuchen Sie bitte [unsere Vereinsseite](https://www.civitasconnect.digital/civitas-core/).

## About
CIVITAS/CORE is the core of an urban data platform. It enables cities, regions and municipal companies to effectively manage any kind of data.

CIVITAS/CORE integrates well with digital infrastructures in urban environments and is designed to be **adaptable and extendable** to the specific needs of a city or region. Thus it's the foundation – in our words: the "CORE" – that provides everything required to manage data in urban environments which can then be extended to a comprehensive urban data platform that is customized to a specific city or region.

CIVITAS/CORE is jointly developed by a community of German cities, regions and municipal companies who join their forces and share the efforts amongst them to reduce costs. This community shares the vision of establishing CIVITAS/CORE as a **technological standard component** for the public sector in Germany.

Following the principle "Public Money – Public Code", CIVITAS/CORE is an **open source software** available for everyone, thereby supporting scaling effects to be quickly and widely adapted. It comes **without operator dependency** – any public or private entity can operate the software itself. Thanks to open source, any operator has full transparency and control about the software which supports finding and fixing security issues early on.

The community of CIVITAS/CORE will soon establish an **ecoysystem** of platform operators, service providers for integrating CIVITAS/CORE into urban infrastructures and for training and consulting services, as well for use cases, data models and connectors. "Built once – used many times" empowers **re-using any type of conceptual or technical building block** that has been created once.

All in all, CIVITAS/CORE can accelerate the implementation of **digital services of general interest ("Digitale Daseinsvorsorge")** at scale by providing full control over the technology which eventually maximises **digital sovereignty** ("Digitale Souveränität") for everyone.

## What is it?
CIVITAS/CORE reuses many popular open source software technologies and combines them in a "clever" way. The following architecture provides an overview of the current version:

<div align="center">
    <br />
    <a>
      <img src="https://gitlab.com/civitas-connect/civitas-core/documentation/-/raw/main/docs/Administration/img/architecture.png" alt="CIVITAS/CORE-architecture">
    </a>
    <br />
    <br />
</div>

Currently, CIVITAS/CORE provides four external APIs:
- OGC SensorThingsAPI by FROST Server
- FIWARE/ETSI NGSI-LD by Stellio Context Broker
- OGC Standard APIs provided by Geoserver
- Prometheus Remote Write by MimirDB

Relevant frontends to show data are:
- Masterportal as geoportal
- Apache Superset for dashboards with IoT data
- Grafana for dashboards for IoT data 

For administration purposes:
- Keycloak is used for user management incl. authentication and authorization.
- Grafana & Loki for monitoring and logging
- Apache APISIX for API management

## How to use
To deploy CIVITAS/CORE you need a running Kubernetes cluster. The deployment is done using an automated script (Ansible playbook) against your Kubernetes cluster. This script reads in a single configuration file, which you adapt to your needs before running it (e.g. you can enable or disable optional platform components).

For details please see the [-> documentation](https://docs.core.civitasconnect.digital).

## Who?
CIVITAS/CORE is collaboratively developed by 13 members of the registered association Civitas Connect e.V. These members consist of [8 cities and regions and 5 municipal companies](AUTHORS-ATTRIBUTION.md), all exclusively in the public sector.
[Civitas Connect e.V.](https://www.civitasconnect.digital/) organises this community and all development activities.

<div align="center">
    <a>
      <img width="700px" src="https://www.civitasconnect.digital/civitas-core-mitglieder-2/" alt="Civitas-Core-Community">
    </a>
</div>

## Releases & Roadmap
Stable versions will be released once per quarter, i.e. 4 times per year. The current state of development can be followed on our [issue board](https://gitlab.com/groups/civitas-connect/civitas-core/-/boards).

The project planning envisages feature development AND maintenance for its newest stable version for at least 8 years until 2032.


**Version 1.0 (current) and later versions (1.x)** will enable cities, regions and municipal companies to implement use cases of low to mid-level complexity. These are use cases that integrate IoT data with geographical data. In simple words: all use cases that visualise sensor data on maps. To learn more about its technical components and features, please see the [documentation](https://docs.core.civitasconnect.digital).

**Version 2.0 and later versions (2.x)** will be a large upgrade. It will enable implementing use cases of any kind of complexity, even for large organisations, and with user interfaces that support users to manage all aspects of their data. The three main features to be developed are:
  - Model-centric approach: managing a single source of truth of datasets for any kind of data and data models by using the abstract concept of "models".
  - User interfaces: manage data  in the platform along all of its lifecycle will be supported with user interfaces.
  - Extensive access management:  Multi-tenancy and fine-grained acess control to comply with complex organisational structures in cities and regions.

The release of version 2.0 is currently planned in the first half of 2025.

## How to contribute
You found a bug, want to report an issue or contribute code or a feature request? [-> Contribution guide](CONTRIBUTION-GUIDE.md)

## Contributors
[-> List of contributors](CONTRIBUTORS.md)

## Reporting Security Issues
Please do not open issues or pull requests - this makes the problem immediately visible to everyone, including malicious actors. Security issues in this open source project can be safely reported to [core@civitasconnect.digital](mailto:core@civitasconnect.digital).

## License
This work is licensed under [EU PL 1.2](LICENSE) by Civitas Connect e. V., Hafenweg 7, 48155 Münster, Germany, and [other authors](https://gitlab.com/civitas-connect/civitas-core/civitas-core/-/blob/main/AUTHORS-ATTRIBUTION.md). 

This project doesn't require a CLA (Contributor License Agreement). The copyright belongs to all the individual contributors.

Please see [here](https://gitlab.com/civitas-connect/civitas-core/civitas-core/-/blob/main/AUTHORS-ATTRIBUTION.md) for previous works this project is based on.
