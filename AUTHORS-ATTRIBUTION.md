# Attribution for authors
In 2019 the initial creators of this work were:
 - [Andreas Linneweber](https://www.kernblick.de/)
 - [Markus Luckey](https://www.kernblick.de/)

Later on between 2020-2023, this work has been further developed further developed as "Urban Dataspace Platform" and "FUTR Hub" in cooperation with:
- State of Berlin, Germany, represented by [Tegel Projekt GmbH](https://www.tegelprojekt.de/)
- The city of Paderborn ([Project 'Digitale Heimat PB'](https://digitale-heimat-pb.de/digitale-heimat/))
- [HYPERTEGRITY AG](https://www.hypertegrity.de/)
- [KERNBLICK GmbH](https://www.kernblick.de/)
- [Frachtwerk GmbH](https://frachtwerk.de/)
- [inno2grid GmbH](https://inno2grid.com/)
- [Q_PERIOR](https://www.q-perior.com/en/)

Since beginning of 2024, this work is further developed as "CIVITAS/CORE" in cooperation with:
- [Civitas Connect e.V.](https://www.civitasconnect.digital/)
- Software development services:
  - [KERNBLICK GmbH](https://www.kernblick.de/)
  - [HYPERTEGRITY AG](https://www.hypertegrity.de/)
- Members of the CIVITAS/CORE Community:
  - [The city of Münster](https://www.stadt-muenster.de/startseite)
  - [Stadtwerke Münster GmbH](https://www.stadtwerke-muenster.de/)
  - [items GmbH](https://itemsnet.de/)
  - [The city of Paderborn](https://www.paderborn.de/)
  - [Westfalen Weser Netz GmbH](https://www.ww-netz.com/)
  - [The city of Jena](https://startseite.jena.de/)
  - [The city of Osnabrück](https://www.osnabrueck.de/de/)
  - [Stadtwerke Osnabrück Netz GmbH](https://www.swo-netz.de/)
  - [The city of Kassel](https://www.kassel.de/)
  - [Kasseler Verkehrs- und Versorgungs-GmbH](https://www.kvvks.de/)
  - [The city of Bamberg](https://www.stadt.bamberg.de/)
  - [The city of Haßfurt](https://hassfurt.de/startseite.html)
  - [The city of Mönchengladbach](https://www.moenchengladbach.de/de/)
  - [The city of Regensburg](https://www.regensburg.de/)
  - [Stadtwerke Regensburg](https://www.das-stadtwerk-regensburg.de/)


# Attribution for other software projects
We clearly stand on the shoulder of several amazing Open Source and Free Software projects. We are very thankful for being able to utilize them for our purposes.

- Configuration & orchestration: 
  - [Kubernetes](https://kubernetes.io/)
  - [Ansible](https://www.ansible.com/)
  - [Gitlab](https://gitlab.com/)
  - [Keel](https://keel.sh/)
- Identity and access: 
  - [Keycloak](https://www.keycloak.org/)
  - [Apache APISIX](https://apisix.apache.org/)
- Logging & monitoring:
  - [Prometheus](https://prometheus.io/)
  - [Loki](https://grafana.com/oss/loki/)
  - [FIWARE QuantumLeap](https://github.com/orchestracities/ngsi-timeseries-api)
- Context
  - [FIWARE Stellio Context Broker](https://github.com/stellio-hub/stellio-context-broker)
  - [FROST-Server](https://github.com/FraunhoferIOSB/FROST-Server)
- Geo data
  - [Masterportal](https://www.masterportal.org/home.html)
  - [Geoserver](https://geoserver.org/)
- Databases
  - [PostgreSQL](https://www.postgresql.org/), [PostGIS](https://postgis.net/), [Timescale](https://www.timescale.com/)
  - [Min.io](https://min.io/)
  - [Mimir](https://grafana.com/oss/mimir/)
- Visualisation 
  - [Grafana](https://grafana.com/)
  - [Apache Superset](https://superset.apache.org/)
- Security
  - [Trivy](https://aquasecurity.github.io/trivy/v0.50/)
- SCM and DevOps
  - [Gitlab Ultimate Edition](https://gitlab.com/)
- and many more...
