# Deploy Platform for Local Deployment
We provide a comprehensive guide for setting up a local deployment platform using Minikube, Nginx Ingress, MetalLB, and Cert-Manager [here](https://docs.core.civitasconnect.digital/docs/next/Deployment/Local-Deployment/).

