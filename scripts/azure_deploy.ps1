# Set the script to stop on errors
$ErrorActionPreference = "Stop"

# Only create if the user confirms
$confirmation = Read-Host "Do you want to create (c) or delete (d) the AKS cluster? (C/d/n)"
if ($confirmation -eq "C" -or $confirmation -eq "c" -or $confirmation -eq "") {

    # 1. Check if the civitas-network resource group exists
    $networkGroupExists = az group exists --name civitas-network | ConvertFrom-Json
    if (-not $networkGroupExists) {
        Write-Host "Creating Azure resource group for networking..." -ForegroundColor Cyan
        az group create --name civitas-network --location germanywestcentral
    }

    # 2. Check if the public IP exists
    $ipExists = az network public-ip list --resource-group civitas-network --query "[?name=='civitas-ingress-ip'].name" --output tsv
    if (-not $ipExists) {
        Write-Host "Creating static public IP..." -ForegroundColor Cyan
        az network public-ip create --resource-group civitas-network --name civitas-ingress-ip --sku Standard --allocation-method static
    }

    # 3. Retrieve the public IP address
    $publicIp = az network public-ip show --resource-group civitas-network --name civitas-ingress-ip --query "ipAddress" --output tsv
    $publicIpId = az network public-ip show --resource-group civitas-network --name civitas-ingress-ip --query "id" --output tsv
    Write-Host "Public IP: $publicIp" -ForegroundColor Cyan

    # 4. Create Azure resource group
    Write-Host "Creating Azure resource group..." -ForegroundColor Cyan
    az group create --name civitas --location germanywestcentral

    # 5. Create AKS cluster with the first node pool using the existing static IP
    Write-Host "Creating AKS cluster..." -ForegroundColor Cyan
    az aks create --resource-group civitas --name civitas --node-count 1 --generate-ssh-keys --node-vm-size Standard_D8s_v3 --load-balancer-outbound-ips $publicIpId

    # Assign Network Contributor Role to AKS Managed Identity (allow accessing IP from civitas-network group)
    Write-Host "Assigning Network Contributor role to AKS Managed Identity (allow accessing IP from civitas-network group)..." -ForegroundColor Cyan
    $clientId = az aks show --resource-group civitas --name civitas --query "identityProfile.kubeletidentity.clientId" --output tsv
    $identityType = az aks show --resource-group civitas --name civitas --query "identity.type" --output tsv
    $subscriptionId = az account show --query "id" --output tsv
    if ($identityType -eq "SystemAssigned") {
        $principalId = az aks show --resource-group civitas --name civitas --query "identity.principalId" --output tsv
    } else {
        $userAssignedIdentity = az aks show --resource-group civitas --name civitas --query "identity.userAssignedIdentities" --output json | ConvertFrom-Json
        $principalId = ($userAssignedIdentity.PSObject.Properties | Select-Object -ExpandProperty Value).principalId
    }
    az role assignment create --assignee $principalId --role "Network Contributor" --scope /subscriptions/$subscriptionId/resourceGroups/civitas-network

    ### CAUTION: If you want to use the following, please make sure you have sufficient VM Quota
    # 6. Add additional node pools
    #Write-Host "Adding Node Pool 2..." -ForegroundColor Cyan
    #az aks nodepool add --resource-group civitas --cluster-name civitas --name nodepool2 --node-count 7 --node-vm-size Standard_D2s_v3
    #Write-Host "Adding Node Pool 3..." -ForegroundColor Cyan
    #az aks nodepool add --resource-group civitas --cluster-name civitas --name nodepool3 --node-count 2 --node-vm-size Standard_D4s_v3

    # 7. Retrieve Kubernetes context
    Write-Host "Fetching AKS credentials..." -ForegroundColor Cyan
    az aks get-credentials --resource-group civitas --name civitas --overwrite-existing

    # 8. Verify that nodes exist
    Write-Host "Checking Kubernetes nodes..." -ForegroundColor Cyan
    kubectl get nodes

    # 9. Install ingress-nginx using Helm with static IP
    Write-Host "Installing Ingress-NGINX with Helm..." -ForegroundColor Cyan
    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
    helm repo update
    helm install ingress-nginx ingress-nginx/ingress-nginx `
        --namespace ingress-nginx `
        --create-namespace `
        --set controller.allowSnippetAnnotations=true `
        --set controller.publishService.enabled=true `
        --set controller.service.externalTrafficPolicy=Local `
        --set controller.replicaCount=1 `
        --set controller.service.loadBalancerIP=$publicIp `
        --set controller.service.annotations."service\.beta\.kubernetes\.io/azure-load-balancer-resource-group"="civitas-network"

    # 10. Verify services in ingress-nginx namespace
    Write-Host "Waiting for LoadBalancer to get an external IP..." -ForegroundColor Cyan
    do {
        Start-Sleep -s 10
        $externalIp = kubectl get svc -n ingress-nginx -o jsonpath='{.items[0].status.loadBalancer.ingress[0].ip}'
        Write-Host "Current LoadBalancer IP: $externalIp" -ForegroundColor Yellow
    } while (-not $externalIp -or $externalIp -eq "<pending>")

    # 11. Install cert-manager using Helm
    Write-Host "Installing cert-manager with Helm..." -ForegroundColor Cyan
    helm repo add jetstack https://charts.jetstack.io --force-update
    helm repo update
    helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.17.0 --set crds.enabled=true

    Write-Host "Deployment completed!" -ForegroundColor Green
}

# If the user does not want to create the AKS cluster
elseif ($confirmation -eq "d" -or $confirmation -eq "D") {

    # Delete Azure resource group in a loop until it's gone
    $groupExists = az group exists --name civitas | ConvertFrom-Json
    while ($groupExists) {
        # Delete AKS cluster
        Write-Host "Deleting AKS cluster..." -ForegroundColor Cyan
        az aks delete --resource-group civitas --name civitas --yes --no-wait

        # Force delete all virtual machines in the resource group
        Write-Host "Force deleting all virtual machines in the resource group..." -ForegroundColor Cyan
        az resource list --resource-group civitas --query "[?type=='Microsoft.Compute/virtualMachines'].name" --output tsv | ForEach-Object {
            az vm delete --resource-group civitas --name $_ --yes --no-wait
        }

        Write-Host "Attempting to force delete Azure resource group..." -ForegroundColor Cyan
        az group delete --name civitas --yes --no-wait
        Start-Sleep -s 30
        $groupExists = az group exists --name civitas | ConvertFrom-Json
    }

    Write-Host "Deletion completed!" -ForegroundColor Green
}

else {
    Write-Host "Operation cancelled or invalid input." -ForegroundColor Yellow
}
