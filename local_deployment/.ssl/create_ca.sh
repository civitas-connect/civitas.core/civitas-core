#!/bin/bash

# Set DOMAIN if not already set
: ${DOMAIN:=civitas.test}
export DOMAIN=${DOMAIN}

BASEDIR=$(dirname $0)

touch ${BASEDIR}/index.txt
echo 01 > ${BASEDIR}/crlnumber

rm -rf ${BASEDIR}/civitas.*

# Generate private key
openssl genrsa -out ${BASEDIR}/civitas.key 2048

# Generate self-signed certificate
openssl req -x509 -new -nodes \
  -key ${BASEDIR}/civitas.key \
  -days 3650 \
  -sha256 \
  -out ${BASEDIR}/civitas.crt \
  -subj "/C=DE/ST=State/L=SmartCity/O=KERNBLICK/OU=CivitasCore/CN=$DOMAIN" \
  -addext "keyUsage=critical, cRLSign, digitalSignature, keyCertSign" \
  -addext "extendedKeyUsage=serverAuth" \
  -addext "subjectAltName=DNS:$DOMAIN,DNS:*.$DOMAIN,IP:127.0.0.1" \
  -addext "crlDistributionPoints=URI:http://$DOMAIN/civitas.crl"

# Generate CRL if needed
# Ensure ca.conf is correctly configured for OpenSSL CA usage
if [ -f ${BASEDIR}/ca.conf ]; then
  cat ${BASEDIR}/ca.conf | envsubst | openssl ca \
    -gencrl \
    -keyfile ${BASEDIR}/civitas.key \
    -cert ${BASEDIR}/civitas.crt \
    -out ${BASEDIR}/civitas.crl.pem \
    -config /dev/stdin
  openssl crl -inform PEM -in ${BASEDIR}/civitas.crl.pem -outform DER -out ${BASEDIR}/civitas.crl
else
  echo "Warning: ca.conf not found. Skipping CRL generation."
fi

