import logging
from typing import List

from superset.security import SupersetSecurityManager

class DataspaceSecurityManager(SupersetSecurityManager):

    def oauth_user_info(self, provider, response=None):
        if provider == 'keycloak':
            me = self.appbuilder.sm.oauth_remotes[provider].get('userinfo')
            data = me.json()
            """ Expected Format:
            {
                # if dataspace (keycloak-group) is assigned
                "tenants":[ # 
                    {
                        "name":"dataspace_1" # 
                    },
                    {
                        "name":"dataspace_2"
                    }
                ],
                # if role (superset client) is assigned
                "resource_access":{
                    "superset":{
                        "roles":[
                            "supersetAdmin" # Only if 
                        ]
                    }
                },
                "name":"Admin Env",
                "preferred_username":"admin@beispiel-stadt.de",
                "given_name":"Admin",
                "family_name":"Env",
                "email":"admin@beispiel-stadt.de"
            }
            """
            return { 
                'id' : data['email'], 
                'email' : data['email'], 
                'name' : data['name'], 
                'username' : data['preferred_username'], 
                'first_name': data['given_name'], 
                'last_name': data['family_name'],
                'roles': [role['name'] for role in data['tenants']],
                'supersetAdmin': 'resource_access' in data and 'superset' in data['resource_access'] and 'roles' in data['resource_access']['superset'] and 'supersetAdmin' in data['resource_access']['superset']['roles']
            }

    def auth_user_oauth(self, userinfo):
        user = super(DataspaceSecurityManager, self).auth_user_oauth(userinfo)
        roles = [self.find_role(x) for x in userinfo['roles']]
        roles = [x for x in roles if x is not None]
        if userinfo['supersetAdmin']:
            roles.append(self.find_role('Admin'))
        roles.append(self.find_role('Gamma'))
        user.roles = roles
        self.update_user(user)  # update user roles
        return user
