---
- name: Setup data-platform and deploy it into K8S cluster
  hosts: localhost
  gather_facts: false

  vars_files:
    - vars/default.yml
    - vars/software_references.yml
    - vars/access/keycloak.yml
    - vars/access/apisix.yml
    - vars/geodata/geodata.yml

  tasks:
    - name: Initialize inventory
      import_tasks: tasks/init_inventory.yml
      tags: [ 'always' ]

    - name: Initialize installation
      import_tasks: tasks/init_installation.yml
      tags: [ 'always' ]

    - name: Setup Velero
      import_tasks: tasks/operation/velero.yml
      when: inv_op_stack.velero.enable
      tags: [ 'ope_velero' ]

    - name: Setup Postgres Operator
      import_tasks: tasks/operation/postgres_operator.yml
      when: inv_op_stack.postgres_operator.enable
      tags: [ 'ope_postgres' ]

    - name: Setup keel.sh
      import_tasks: tasks/operation/keel.yml
      when: inv_op_stack.keel_operator.enable
      tags: [ 'ope_keel' ]

    - name: Setup Central Database
      import_tasks: tasks/operation/postgrescluster.yml
      when: inv_central_db.enable
      tags: [ 'ope_central_db' ]

    - name: Setup Keycloak Client
      import_tasks: tasks/access/keycloak.yml
      when: inv_access.keycloak.enable and inv_access.enable
      tags: [ 'acc_keycloak' ]

    - name: Setup Service Portal
      import_tasks: tasks/access/service_portal.yml
      when: inv_access.service_portal.enable and inv_access.enable
      tags: [ 'acc_service_portal' ]

    - name: Setup Monitoring Stack 
      import_tasks: tasks/operation/monitoring.yml
      when: inv_op_stack.monitoring.enable 
      tags: [ 'ope_monitoring']

    - name: Setup APISIX System
      import_tasks: tasks/access/apisix.yml
      when: inv_access.apisix.enable and inv_access.enable
      tags: [ 'acc_apisix' ]

    - name: Setup Grafana
      import_tasks: tasks/dashboard/grafana.yml
      when: inv_da.grafana.enable
      tags: [ 'da_grafana' ]

    - name: Setup Superset
      import_tasks: tasks/dashboard/superset.yml
      when: inv_da.superset.enable
      tags: [ 'da_superset' ]

    - name: Setup Frost Server
      import_tasks: tasks/context/frost.yml
      when: inv_cm.frost.enable
      tags: [ 'cm_frost' ]

    - name: Setup Mimir
      import_tasks: tasks/context/mimir.yml
      when: inv_cm.mimir.enable
      tags: [ 'cm_mimir' ]

    - name: Setup Stellio Server
      import_tasks: tasks/context/stellio.yml
      when: inv_cm.stellio.enable
      tags: [ 'cm_stellio' ]

    - name: Setup Quantumleap Server
      import_tasks: tasks/context/quantumleap.yml
      when: inv_cm.quantumleap.enable
      tags: [ 'cm_quantumleap' ]

    - name: Setup Geostack Components
      import_tasks: tasks/geodata/geodata.yml
      when: inv_gd.enable
      tags: [ 'geodata' ]

    - name: Setup PGAdmin
      import_tasks: tasks/operation/pgadmin.yml
      when: inv_op_stack.pgadmin.enable
      tags: [ 'ope_pgadmin' ]

    - name: Setup APIs in API Management
      import_tasks: tasks/access/apis.yml
      when: inv_access.apis.import and inv_access.enable and inv_access.apisix.enable
      tags: [ 'acc_apis' ]

    - name: Include addons
      include_tasks: "{{ item }}"
      with_fileglob: "{{ inv_addons.addons }}"
      when: inv_addons and inv_addons.import
      tags: [ 'addons' ]
