# How to use

This document will describe how to perform the tests locally. In the future we will add a guide on how to implement the tests in GitLab CI. 

## Step 1: Build Docker Image

First, you need to build the Docker image. To do this, run the following command in the same folder as this README:

```bash
docker build -t karate-api-tester .
```

## Step 2: Run the Docker Image

After building the image, you can run the tests. To do this, run the following command:

```bash
docker run -it --rm -v "$(pwd)":/src -w /src -e KEYCLOAK_BASE_URL='<keycloak-base-url>' -e REALM='<realm-name>' -e API_ACCESS_CLIENT_SECRET='<api-access-client-secret>' -e TESTUSER_USERNAME='<testuser-username>' -e TESTUSER_PASSWORD='<testuser-password>' -e STELLIO_API_URL='<stellio-url>' -e FROST_API_URL='<frost-server-url>' -e TEST_DATASPACE='<test-dataspace>' -e GEOSERVER_API_URL='<geoserver-url>'  karate-api-tester java -jar /karate.jar .
```